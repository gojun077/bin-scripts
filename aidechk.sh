#!/bin/bash
# aidecheck.sh

# Last Updated: 2016-11-24
# Jun Go gojun077@gmail.com

# This script must be executed as root. It does the following:
# - backup the existing 'aide.db.gz' if it exists
# - rename the new scan results as 'aide.db.gz'
# - execute 'aide --check'

# Timestamp in YYYY-MM-DD_HH.MM format
timestamp=$(date +%F_%H.%M)
newfile="/var/lib/aide/aide.db.new.gz"
defaultf="/var/lib/aide/aide.db.gz"
backup="/var/lib/aide/aide-$timestamp.db.gz"

# Rename existing DB file to aide-YYYY-MM-DD_HH.MM
if [ -f $defaultf ]; then
  printf "%s\n" "### Backing up existing AIDE DB to $backup ###"
  mv $defaultf $backup
else
  printf "%s\n" \
         "$defaultf does not exist. You may need to rename $newfile"
  printf "%s\n" \
         "Have you run 'aide --init' at least once?"
fi

# The following conditional block is needed when you run
# 'aide --init' or 'aide --update'
if [ -f $newfile ]; then
  printf "%s\n" "### Renaming $newfile to $defaultf  ###"
  mv $newfile $defaultf
fi

printf "%s\n" "### Running 'aide --check' ###"
aide --check > /var/log/aide/check.out
