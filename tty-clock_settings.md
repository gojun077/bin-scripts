Jun's tty-clock settings
=========================
`tty-clock` is an ncurses terminal clock

# I invoke it with the following options:

`tty-clock -C 5 -b -c -s`

## The meaning the options above:
- `-C 5` color (0~7) '5' is purple
- `-b` use bold colors
- `-c` center the clock in the term
- `-s` show seconds

## Other options I sometimes use
- `r` clock rebounds from the edges of the term window
- `B` enable blinking colon
