bin
================
Repo of various personal scripts for Linux (mostly Arch). Make sure to clone repo `bin-scripts` into `$HOME/bin`

**LICENSE**
I feel this isn't necessary, but just in case these scripts use the MIT License.
