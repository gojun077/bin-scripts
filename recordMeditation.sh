#!/bin/bash
# recordMeditation.sh

# This script launches 'motion' webcam capture program, and once motion
# terminates, the script will join individual webcam captures into a
# mpeg4 movie. Each webcam shot is taken after an interval of 6 seconds
# as set by /etc/motion/motion.conf "snapshot_interval n"

DATE=$(date +%F-%H%M)
VIDPATH="/usr/local/apache2/htdocs/cam1"
if [ -f "/usr/bin/motion" ]; then
  motion
else
  echo -e "Please install 'motion' package! Aborting.\n" 1>&2
  exit 1
fi

if [ -d $VIDPATH ]; then
  cd $VIDPATH || exit 1
  if ffmpeg -framerate 1/1 -pattern_type glob -i \
    "*snapshot.jpg" -r 25 "$DATE".mp4; then
    rm ./*.jpg
    if [ -f "$HOME"/bin/meditationBeeminder.sh ]; then
      bash "$HOME"/bin/meditationBeeminder.sh
    else
      echo "Cannot find script. Email to Beeminder not sent." 1>&2
      exit 1
    fi
  else
    echo "Could not render mp4! Aborting." 1>&2
    exit 1
  fi
else
  echo "$VIDPATH does not exist! Aborting." 1>&2
  exit 1
fi

