#!/bin/bash
# vpn_firewall.sh

# This script will open ports in firewalld required for
# VPN traffic.

# USAGE
# sudo ./vpn_firewall.sh <open/close>
# where <parameter> can be either 'open' or 'close'
# Parameter 'open' will cause the port or service to be
# added to the firewalld whitelist, while 'close' will
# cause them to be closed

DZONE=public

if [ "$1" = "open" ]; then
  CMD=add;
elif [ "$1" = "close" ]; then
  CMD=remove
else
  printf "%s\n" "Parameter must be either 'open' or 'close'" 1>&2
  exit 1
fi


#################
#   UDP PORTS
#################
# Internet Key Exchange
IKE=500
# IPSec NAT (NAT-T)
NATT=5500
# L2TP
L2TP=1701

UDPPORTS=($IKE
          $NATT
          $L2TP
)

# ENABLE UDP PORTS
for i in ${UDPPORTS[*]}; do
  firewall-cmd --zone="$DZONE" --$CMD-port="$i"/udp
done
