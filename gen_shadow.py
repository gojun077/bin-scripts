#!/usr/bin/python3
# gen_shadow.py
# Created on: 2017.04.13
# Created by: gojun077@gmail.com
# Last Updated: 2021.03.30
#
# Generates a salted SHA512 hash of a Linux user password entered by the user
# that can be used in /etc/shadow. Note that crypt.crypt() on non-Linux OS'es
# will generate a hash that won't work in Linux /etc/shadow!


import crypt
import getpass
import sys


if sys.platform != "linux":
    print(f"This script requires Linux! You are on {sys.platform}. Aborting...")
    sys.exit(1)

mypass = getpass.getpass("Enter PW to be salted and hashed with SHA512: ")
print(crypt.crypt(mypass, crypt.mksalt(crypt.METHOD_SHA512)))
