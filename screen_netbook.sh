#!/bin/bash

#screen_netbook.sh
#This script launches Screen with various tabs and starts various programs
#such as htop, cmus, etc. as well as custom scripts in separate tabs.
#It is intended to be used in tty-only sessions on netbooks that don't
#have enough memory and CPU to handle X11 graphical sessions

# Meaning of GNU Screen option flags:
# -A    adapt the sizes of all windows to the size of the current terminal
# -d -m launch GNU Screen in detached mode; creates a new session but
#       doesn't attach to it
# -S    When creating new session, specify session name
# -p    preselect a window (by no. or name)
# -X    send cmd to running screen session

# to list running Screen sessions, 'screen -ls'
# to reconnect to a Screen session, 'screen -r sessionName'

# launch a GNU Screen session named 'netbook' in detached mode
screen -AdmS netbook        -t HOME  # tab0
# create separate tabs in session 'netbook'
screen -S netbook -X screen -t HTOP   # tab 1
screen -S netbook -X screen -t DMESG  # tab 2
screen -S netbook -X screen -t MKC    # tab 3
screen -S netbook -X screen -t MC     # tab 4
screen -S netbook -X screen -t IRSSI  # tab 5


# tab 1 ncurses top
screen -S netbook -p 1  -X stuff "htop^M"
# tab 2 follow dmesg kernel ring buffer
screen -S netbook -p 2  -X stuff "sudo dmesg -wH^M"
# tab 3 Musikcube mpd server
screen -S netbook -p 3  -X stuff "musikcube^M"
# tab 4 Midnight Commander file manager
screen -S netbook -p 4  -X stuff "mc^M"
# tab 5 IRSSI IRC client
screen -S netbook -p 5  -X stuff "irssi^M"

#screen -S netbook -p 0 -X stuff "./tty_startup.sh^M"
