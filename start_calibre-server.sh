#!/bin/bash
# start_calibre-server.sh
#
# Created on: Apr 17 2023
# Created by: gopeterjun@naver.com
# Last Updated: May 9 2023
#
# Launch `calibre-server` with various command-line options from
# server in my homelab

CLBR_PATH=/tier2/calibre
CLBR_LIB=/tier2/Calibre_Library

if pgrep -a -f "/usr/bin/calibre-server" > /dev/null; then
  printf "%s \\n" "calibre-server is already running!"
  exit 1
fi

calibre-server --access-log /tier2/calibre/access.log \
  --log ${CLBR_PATH}/server.log --max-log-size 50 \
  --port 9080 --userdb ${CLBR_PATH}/users.sqlite --enable-auth --daemonize \
  ${CLBR_LIB}
