#!/bin/bash
# put up Ethernet interface and assign IP for Lenovo S310
# this script must be run as root

ip l set enp1s0 up
ip a flush enp1s0
ip a add 192.168.95.97/24 broadcast 192.168.95.255 dev enp1s0
ip a show up
