#/bin/bash
# This script adds an undetected resolution to an external
# Samsung LCD monitor. When connected via VGA, this monitor
# does not send its 1920x1080 resolution via EDID. I am
# therefore adding this resolution manually with xrandr.
# Output settings for 1920x1080 were obtained from the
# results of 'cvt -r 1920 1080'
# Finally, the orientation of two screens is set, with
# the Samsung LCD monitor on the left and the notebook
# monitor on the right
xrandr --newmode "1920x1080R" 138.50 1920 1968 2000 2080 1080 1083 1088 1111 +hsync -vsync
xrandr --addmode VGA1 1920x1080R
xrandr --output VGA1 --mode 1920x1080R
arandr-samsung-work-vga.sh
