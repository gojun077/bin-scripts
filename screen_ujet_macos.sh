#!/bin/bash
# screen_ujet.sh
#
# Created on: Apr 30 2024
# Created by: gopeterjun@naver.com
# Last Updated: Jun 9 2024
#
# Set up a GNU Screen work environment for UJET

screen -AdmS ujet          -t cmus   # tab 0
screen -S ujet    -X screen -t sh0    # tab 1
screen -S ujet    -X screen -t sh1    # tab 2
screen -S ujet    -X screen -t sh2    # tab 3
screen -S ujet    -X screen -t sh3    # tab 4
screen -S ujet    -X screen -t rmt0   # tab 5
screen -S ujet    -X screen -t rmt1   # tab 6
screen -S ujet    -X screen -t util   # tab 7
screen -S ujet    -X screen -t ovpn   # tab 8

# tab 0
screen -S ujet -p 0 -X stuff "cmus^M"
# tab 7: 'keybase-redirector' only exists on Linux, not MacOS
screen -S ujet -p 7 -X stuff "#sudo keybase-redirector /keybase"
# tab 8: do not send newline; 2FA login requires browser auth by human
screen -S ujet -p 8 -X stuff "# ovpncli config-import --config foo.ovpn"
