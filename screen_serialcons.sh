#!/bin/bash
# screen_serialcons.sh
#
# Use GNU Screen to connect to serial console for ATCA servers,
# network switches, etc. To run this script as regular user, make
# sure $USER is a member of the groups 'tty' and 'uucp'

ttydevice=$1
bps=$2

if [ $# -eq 0 ]; then
    printf "%s\n" "USAGE: ./screen_serialcons.sh [serialConsoleDev] [bps]"
    printf "%s\n" "ex: ./screen_serialcons.sh /dev/ttyS0 115200"
    printf "%s\n" "If baud rate is not specified, Screen defaults to 9600"
    exit 1
fi

screen "$ttydevice" "$bps"
