#!/usr/bin/env bash
#
# sway_headless.sh
# Created on: Dec 19 2022
# Last Updated: Dec 19 2022
#
# This script should be launched inside of a terminal multiplexer like
# `tmux` or `screen`.
# It launches a wayland desktop session on a headless machine and also
# starts `wayvnc` daemon so you can connect to Wayland over VNC from any
# VNC client

export WLR_BACKENDS=headless
export WLR_LIBINPUT_NO_DEVICES=1
export WAYLAND_DISPLAY=wayland-1
export XDG_RUNTIME_DIR=/tmp
export XDG_SESSION_TYPE=wayland
sway &
wayvnc -p 0.0.0.0
