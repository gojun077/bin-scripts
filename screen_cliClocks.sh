#!/bin/bash
# screen_cliClocks.sh
#
# Created on: Mar 24 2021
# Created by: gojun077@gmail.com
# Last Updated: Jul 17 2023
#
# Sets up a GNU Screen work environment with horizontally and
# vertically-split windows with several instances of tty-clock
# with TZ set to various timezones

screen -AdmS cliClocks           -t MTL      # tab 0
screen -S cliClocks    -X screen -t CPH      # tab 1
screen -S cliClocks    -X screen -t TEX      # tab 2
screen -S cliClocks    -X screen -t SFO      # tab 3

# tab 0
screen -S cliClocks -p 0 -X stuff "TZ='America/New_York' tty-clock -cC1 -f \"%a, %d %b %Y %T %z\"^M"
# tab 1
screen -S cliClocks -p 1 -X stuff "TZ='Europe/Copenhagen' tty-clock -cC5 -f \"%a, %d %b %Y %T %z\"^M"
# tab 2
screen -S cliClocks -p 2 -X stuff "TZ='America/Mexico_City' tty-clock -cC3 -f \"%a, %d %b %Y %T %z\"^M"
# tab 3
screen -S cliClocks -p 3 -X stuff "TZ='America/Vancouver' tty-clock -cC4 -f \"%a, %d %b %Y %T %z\"^M"
# create screen layout
screen -S cliClocks      -X layout new layout_clocks
screen -S cliClocks      -X select 0
# NOTE: the screen splits below are currently not working, although
# if I type the commands into a screen session they work fine
screen -S cliClocks      -X split
screen -S cliClocks      -X split -v
screen -S cliClocks      -X focus right
screen -S cliClocks      -X select 1
screen -S cliClocks      -X focus bottom
screen -S cliClocks      -X split -v
screen -S cliClocks      -X select 2
screen -S cliClocks      -X focus right
screen -S cliClocks      -X select 3
screen -S cliClocks      -X layout save layout_clocks
