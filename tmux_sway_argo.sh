#! /usr/bin/env bash
# tmux_sway_argo.sh
#
# Created by: gojun077@gmail.com
# Created on: 23 Feb 2023
# Last Updated: Sat 06 Jan 2025
#
# This is a startup script that sets up a tmux multiplexer envo with
# different tabs each running a different application. One tab
# launches a `sway` Window Manager session in Wayland and then runs
# `wayvnc` to accept VNC remote desktop connections. Another tab
# launches `musikcube` audio streaming server so I can stream my `mp3`
# collection to other devices.  Finally a few tabs for local bash
# shell and remote k3s nodes are created.


session="argosway"

# check for other tmux sessions with the same name
if (tmux list-sessions | grep -q "$session"); then
  printf "%s\\n" "tmux session $session already exists!"
  exit 1
fi

# tmux is not sourcing my ~/.bash_profile so I added this
source "$HOME/.bash_profile"
printf "%s\\n" "launching tmux session $session in detached mode..."
tmux new -s "$session" -d

printf "%s\\n" "setting up window 0..."
tmux rename-window -t 0 'sway'
# Setup main window 'sway'
tmux send-keys -t 'sway' 'export WLR_BACKENDS=headless' C-m
tmux send-keys -t 'sway' 'export WLR_LIBINPUT_NO_DEVICES=1' C-m
tmux send-keys -t 'sway' 'export WAYLAND_DISPLAY=wayland-1' C-m
tmux send-keys -t 'sway' 'export XDG_RUNTIME_DIR=/run/user/1000' C-m
tmux send-keys -t 'sway' 'export XDG_SESSION_TYPE=wayland' C-m
tmux send-keys -t 'sway' 'export GTK_IM_MODULE=fcitx' C-m
tmux send-keys -t 'sway' 'export QT_IM_MODULE=fcitx' C-m
tmux send-keys -t 'sway' 'sway &' C-m
tmux send-keys -t 'sway' 'wayvnc -p 0.0.0.0' C-m
tmux send-keys -t 'sway' 'sleep 5' C-m 'echo ' C-m
# re-launch 'wayvnc' if it fails to come up
tmux send-keys -t 'sway' 'if ! pgrep wayvnc; then wayvnc -p 0.0.0.0; fi' C-m
tmux send-keys -t 'sway' 'sleep 3' C-m 'echo ' C-m
tmux send-keys -t "sway" "date +'%F %H:%M:%S'" C-m

# Note: if mouse clicks stop working in the wayvnc session, try
# restarting the sway session.

printf "%s\\n" "setting up window 1..."
# Create and setup pane for musikcube / streaming server
tmux new-window -t $session:1 -n 'musik'
tmux send-keys -t 'musik' 'musikcube' C-m

printf "%s\\n" "setting up windows 2, 3..9"
# Create panes for bash shell
tmux new-window -t $session:2 -n 'sh0'
tmux send-keys -t 'sh0' 'for i in {4..10}; do tmux send-keys -t $i C-m; done'
tmux new-window -t $session:3 -n 'sh1'
tmux send-keys -t 'sh1' 'ssh-add ~/.ssh/archjun-newgen' # `C-m` not sent
# Create panes for remotes
tmux new-window -t $session:4 -n 'ag0'
tmux send-keys -t 'ag0' 'ssh agent0'  # don't send <ENTER> with `C-m`
tmux new-window -t $session:5 -n 'ag1'
tmux send-keys -t 'ag1' 'ssh agent1' # don't send <ENTER> with `C-m`
tmux new-window -t $session:6 -n 'ag2'
tmux send-keys -t 'ag2' 'ssh agent2' # don't send <ENTER> with `C-m`
tmux new-window -t $session:7 -n 'srv0'
tmux send-keys -t 'srv0' 'ssh server0' # don't send <ENTER> with `C-m`
tmux new-window -t $session:8 -n 'srv1'
tmux send-keys -t 'srv1' 'ssh server1' # don't send <ENTER> with `C-m`
tmux new-window -t $session:9 -n 'srv2'
tmux send-keys -t 'srv2' 'ssh server2' # don't send <ENTER> with `C-m`
tmux new-window -t $session:10 -n 'rpi3'
tmux send-keys -t 'rpi3' 'ssh rpi3b' # don't send <ENTER> with `C-m`
tmux new-window -t $session:11 -n 'k9s'
tmux send-keys -t 'k9s' 'k9s'  # launch k9s IDE for kubernetes
# after entering ssh key password to `ssh-agent` use
# `tmux send-keys -t <N> C-m` to send <ENTER> to each of the ssh tabs
# to connect to k3s nodes and devices in your homelab
tmux new-window -t $session:12 -n 'utils'
tmux send-keys -t 'utils' '# utils' C-m
tmux new-window -t $session:13 -n 'enc'
tmux send-keys -t 'enc' 'sudo fscrypt unlock /tier2/encrypted_repos'
