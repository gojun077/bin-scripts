#!/bin/bash
#
# screen_u36jc.sh
#
# Last Updated: Jan 23 2022
# Updated by: gojun077@gmail.com
#
# Sets up a GNU Screen environment for my U36JC notebook

screen -AdmS u36jc           -t home
# Create named tabs
screen -S u36jc    -X screen -t pimax  # tab 1 'RPi4B 8GB pimax'
screen -S u36jc    -X screen -t pj0    # tab 2 'RPi4B 4GB pj2'
screen -S u36jc    -X screen -t pj1    # tab 3 'RPi4B 8GB pj3'
screen -S u36jc    -X screen -t irssi  # 'irssi IRC client'
screen -S u36jc    -X screen -t music  # 'musikcube, cmus, etc'
screen -S u36jc    -X screen -t loc0   # tab 6
screen -S u36jc    -X screen -t loc1   # tab 7
screen -S u36jc    -X screen -t rmt0   # tab 8
screen -S u36jc    -X screen -t rmt1   # tab 9

# Launch programs in tabs
screen -S u36jc -p 1   -X stuff "ssh pimax^M"
screen -S u36jc -p 2   -X stuff "ssh pj0^M"
screen -S u36jc -p 3   -X stuff "ssh pj1^M"
screen -S u36jc -p 4   -X stuff "irssi^M"
screen -S u36jc -p 5   -X stuff "cmus^M"
screen -S u36jc -p 6   -X stuff "echo 'local terminal'^M"
screen -S u36jc -p 7   -X stuff "echo 'local terminal'^M"
screen -S u36jc -p 8   -X stuff "echo 'remote terminal'^M"
screen -S u36jc -p 9   -X stuff "echo 'remote terminal'^M"

