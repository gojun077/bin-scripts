#!/bin/bash
#
# screen_work.sh
#
# Last Updated: 2020.01.17
# Updated by: gojun077@gmail.com
#
# Sets up a a GNU Screen work environment for WSL complete with
# named tabs

screen -AdmS work        -t home  # tab 0 'home'
screen -S work    -X screen -t fifa  # tab 1
screen -S work    -X screen -t pict  # tab 2
screen -S work    -X screen -t macbook  # tab 3 macbook
screen -S work    -X screen -t cronlog   # tab 4 'cronlog'
screen -S work    -X screen -t local0  # tab 5 'workstation local0'
screen -S work    -X screen -t local1    # tab 6 'local1'
screen -S work    -X screen -t local2    # tab 7 'local2'
screen -S work    -X screen -t hobo  # tab 8 vagrant
screen -S work    -X screen -t macbook2  # tab 9 macbook
screen -S work    -X screen -t bitlbee  # tab 10 'bitlbee'
screen -S work    -X screen -t irssi   # tab 11 'irssi IRC client'

# tab 1
screen -S work -p 1 -X stuff "cd ~/Documents/eak-fifa-devops/ea-fifa-devops-pj^M"
# tab 2
screen -S work -p 2 -X stuff "cd /mnt/c/Users/pekoh/Pictures^M"
# tab 3
screen -S work -p 3 -X stuff "sudo service cron start^M"
# tab 4
screen -S work -p 4 -X stuff "tail -f /tmp/crontab.log^M"
# tab 8 vagrant
screen -S work -p 8 -X stuff "cd /mnt/d/vagrantBoxen^M"
# launch bitlbee daemon
screen -S work -p 10 -X stuff "echo 'bitlbee tab'^M"
# launch irssi
screen -S work -p 11 -X stuff "echo 'irssi tab'^M"
