#!/bin/bash
# ufw_pxe.sh
# Created by Jun Go gojun077@gmail.com
# Last Updated 2016-07-12

# This script will open ports needed for
# running a PXE server

# This script must be run as root

# This script has been tested on an Ubuntu 16.04
# server.

# USAGE
# sudo ./firewalld_pxe.sh <parameter>
# where <parameter> can be either 'open' or 'close'
# Parameter 'open' will cause ports or services to be
# added to the ufw whitelist, while 'close' will
# cause them to removed

if [ "$1" = "open" ]; then
  CMD=allow
elif [ "$1" = "close" ]; then
  CMD=deny
else
  echo -e "Parameter must be either 'open' or 'close'\n" 1>&2
  exit 1
fi


#################
#   TCP PORTS
#################
# darkhttpd
DARK1=8080
DARK2=8000
# ssh (this is enabled by default; no need to add)
#SSH=22

TCPPORTS=($DARK1
          $DARK2
         )


#################
#   UDP PORTS
#################
# BOOTP and DHCP use the same UDP port numbers
# UDP 67 is the destination port of a server
# UDP 68 is used by the client
DHCPSERV=67
DHCPCLIENT=68

UDPPORTS=($DHCPSERV
          $DHCPCLIENT
         )


#################
#   SERVICES
#################
# On Ubuntu refer to /etc/services for a list of
# recognized services and their TCP/UDP ports
SERVICES=(
          http   #80/tcp/udp
          tftp   #69/udp
         )

#################
#   MAIN PROG
#################

# ENABLE SERVICES REQ'D FOR PXE
for i in ${SERVICES[*]}; do
  ufw $CMD "$i"
done

for j in ${TCPPORTS[*]}; do
  ufw $CMD "$j"/tcp
done

for k in ${UDPPORTS[*]}; do
  ufw $CMD "$k"/udp
done

# List open ports
ufw status verbose
