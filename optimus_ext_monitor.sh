#!/bin/bash
# optimus_ext_monitor.sh
# Jun Go gojun077@gmail.com
# Last Updated: 2016-12-02

# This script launches the bumblebee daemon and starts
# intel-virtual-output in foreground mode so that external
# monitors connecting to the HDMI port will be automatically
# detected by Xorg graphical desktop.

sudo systemctl start bumblebeed

if [ -f /usr/bin/intel-virtual-output ]; then
  intel-virtual-output -f
else
  printf "%s\n" "intel-virtual-output is not installed!"
  exit 1
fi
