#!/bin/bash
#
# screen_whalex.sh
#
# Last Updated: 2018.07.24
# Updated by: gojun077@gmail.com
#
# Sets up a a GNU Screen work environment complete with named
# tabs for WhaleX.

screen -AdmS whalex           -t local0  # tab 0 'local0'
screen -S whalex    -X screen -t local1  # tab 1 'local1'
screen -S whalex    -X screen -t air0    # tab 2 'sen0'
screen -S whalex    -X screen -t air1    # tab 3 'sen1'
screen -S whalex    -X screen -t hobo0   # tab 4 'val0'
screen -S whalex    -X screen -t aws0    # tab 5 'AWS instance0'
screen -S whalex    -X screen -t aws1    # tab 6 'AWS instance1'
screen -S whalex    -X screen -t aws2    # tab 7 'AWS instance2'
screen -S whalex    -X screen -t aws3    # tab 8 'AWS instance3'
screen -S whalex    -X screen -t irssi   # tab 9 'AWS irssi IRC'

# tab 2
screen -S whalex -p 2 -X stuff "echo 'hello world'^M"
# tab 3
screen -S whalex -p 3 -X stuff "echo 'hello world'^M"
# tab 4
screen -S whalex -p 4 -X stuff "echo 'hello world'^M"
# tab 9, launch irssi
screen -S whalex -p 9 -X stuff "irssi^M"

# Move current tab to tab 0
screen -S whalex -X "next"  # doesn't work
# Vertical Split Windows
#screen -S whalex -X "split -v"  # doesn't work
# Save GNU Screen window layout
#screen -S whalex -X "layout save foo"  # doesn't work

# TODO
# The lines with "doesn't work" comments will work if the commands
# are placed within a .screenrc file instead of being run from a
# script; But I want to achieve the same without hardcoding my
# GNU Screen layout within .screenrc ...
