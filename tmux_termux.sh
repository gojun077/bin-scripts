#! /usr/bin/env bash
# tmux_termux.sh
#
# Created by: gojun077@gmail.com
# Created on: Apr 13 2023
# Last Updated: Jul 03 2024
#
# This is a startup script that sets up a tmux multiplexer envo for termux
# terminal on Android


session="tmuxtermux"

# check for other tmux sessions with the same name
if (tmux list-sessions | grep -q "$session"); then
  printf "%s\\n" "tmux session $session already exists!"
  exit 1
fi

printf "%s\\n" "launching tmux session $session in detached mode..."
tmux new -s "$session" -d

printf "%s\\n" "setting up window 0..."
tmux rename-window -t 0 'sshd'
# Setup main window 'sshd'
tmux send-keys -t 'sshd' \
  'if ! pgrep -u "$USER" sshd > /dev/null; then sshd; fi' C-m
# sshd on termux runs in bg by default

printf "%s\\n" "setting up window 1..."
# Create and setup pane for local terminal
tmux new-window -t $session:1 -n 'loc0'
tmux send-keys -t 'loc0' 'printf "%s\\n" "This is pane 1"' C-m

printf "%s\\n" "setting up window 2..."
# Create and setup pane for argonaut ssh with `ssha` termux wrapper
tmux new-window -t $session:2 -n 'argo'
tmux send-keys -t 'argo' 'ssha argonaut' # don't send <ENTER> with `C-m`

printf "%s\\n" "setting up window 3..."
# Create and setup pane for pj0 ssh
tmux new-window -t $session:3 -n 'pj0'
tmux send-keys -t 'pj0' 'ssha pj0'

printf "%s\\n" "setting up window 4..."
# Create and setup pane for pj1 ssh
tmux new-window -t $session:4 -n 'pj1'
tmux send-keys -t 'pj1' 'ssha pj1'

printf "%s\\n" "setting up window 5..."
# Create and setup pane for pimax ssh
tmux new-window -t $session:5 -n 'pimax'
tmux send-keys -t 'pimax' 'ssha pimax'
