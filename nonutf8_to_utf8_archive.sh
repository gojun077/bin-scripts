#!/bin/bash
# nonutf8_to_utf8_archive.sh
# Last Updated: 2016-12-04
# Jun Go gojun077@gmail.com

# This script uses 7za and convmv to decompress an archive containing
# filenames with non-UTF8 encoding such as 'euc-kr' or 'cp949'
# which is used only on MS Windows, and convert the filenames to
# UTF8 so they will display properly on POSIX systems.

archive=$1
nonutf8=$2
extractp=$(basename "$1" | cut -d. -f1)

if [ $# -ne 2 ]; then
  printf "%s\n" "USAGE:"
  printf "%s\n" \
         "./nonutf8_to_utf8_archive.sh [archive file] [non-UTF8 encoding]"
  printf "%s\n" "ex) ./nonutf8_to_utf8_archive.sh my.zip euc-kr"
  exit 1
fi

if [ -f /usr/bin/7za -a -f /usr/bin/convmv ]; then
  env LANG=C 7za x -o"$extractp" "$archive"
  convmv -f "$nonutf8" -t utf8 -r --notest "$extractp"
else
  printf "%s\n" "Make sure both p7zip and convmv are installed."
  exit 1
fi

