#!/usr/bin/env bash
# golang_upgrade_macos.sh
#
# Created by: gojun077@gmail.com
# Created on: Dec 12 2021
# Last Updated: Jan 18 2024
#
# This script updates an existing Golang install that was setup with
# 'golang_install*.sh'.

set -e
gourl="$1"


if [ $# -ne 1 ]; then
  printf "%s\\n" "### Please pass 1 argument to this script! ###"
  printf "%s\\n" "1: url for downloading golang tarball for Darwin"
  exit 1
fi


gopkg=$(basename "$gourl")
checkos=$(python3 -c "import platform;mysys=platform.system();print(mysys)")
arch=$(python3 -c "import platform;myarch=platform.machine();print(myarch)")

if [[ "$checkos" == "Darwin" && "$arch" == "x86_64" ]]; then
  :
else
  printf "%s\\n" "### This script should only be run on MacOS x86_64! ###"
  exit 1
fi

if ! [ "$USER" = "root" ]; then
  printf "%s\\n" "### This script must be executed as ROOT! ###"
  exit 1
fi

if ! type wget &> /dev/null; then
  printf "%s\\n" "Please install wget before running this script"
  exit 1
fi

function download() {
  wget "$gourl" -P /tmp
}

function upgrade() {
  printf "%s\\n" "### Deleting Old Golang pkgs from '/usr/local/go' ###"
  rm -rf /usr/local/go

  printf "%s\\n" "### Extracting New Golang pkgs to '/usr/local/go' ###"
  tar -xvf "/tmp/$gopkg" -C /usr/local/
}

# Only download tarball if it doesn't exist yet
if ! [ -f "/tmp/$gopkg" ]; then
  download
  upgrade
else
  printf "%s\\n" "$gopkg already exists in /tmp!"
  read -r -p "Do you still want to upgrade? (y/n)" confirm
  if [[ $confirm == [yY] ]]; then
    upgrade
  else
    printf "%s\\n" "Exiting..."
  fi
fi
