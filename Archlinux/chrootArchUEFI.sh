#!/bin/bash
# chrootArchUEFI.sh
# Last Updated: 2016-12-27
# Jun Go gojun077@gmail.com

# This script should be run after 'installArchUEFI.sh'
# within the installation chroot. In the chroot,
# /mnt is mapped to /, so to scp this script to the
# chroot env, you must do the following:
#
# scp chrootArch.sh root@10.10.10.97:/mnt/
#
# Then the script should appear in the chroot '/'

pxeserver="10.10.10.97"

printf "%s\n" "#### Set hostname ####"
echo pcArch > /etc/hostname
printf "%s\n" "#### Set timezone ####"
ln -s /usr/share/zoneinfo/Asia/Seoul /etc/localtime


printf "%s\n"   "###############################"
printf "%s\n"   "###  UEFI Bootloader Setup  ###"
printf "%s\n\n" "###############################"
bootctl install
printf "%s\n" "#### Create /boot/loader/loader.conf ####"
# Basic systemd-boot config is kept in loader.conf located on
# ESP (EFI System Part) in path '/boot/loader/'

cat <<EOF>/boot/loader/loader.conf
timeout 5
default arch
EOF

printf "%s\n" "#### Create /boot/loader/entries/arch.conf ####"
cat <<EOF>/boot/loader/entries/arch.conf
title Archlinux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
options root=/dev/mapper/ARCH-rootvol rw
EOF


printf "%s\n"   "###############################"
printf "%s\n"   "###   Install add'l  pkgs   ###"
printf "%s\n\n" "###############################"
printf "%s\n" "#### Install text mode packages ####"
pacman -S --noconfirm screen mc vim cronie dialog wpa_supplicant \
       libnl git cmus mplayer fbida p7zip alsa-utils alsa-tools abs \
       emacs gdb shellcheck ipython2 ipython bitlbee ncdu \
       android-tools android-udev htop irssi dhclient openssh \
       firewalld nfs-utils vsftpd rsync docker intel-ucode auctex \
       archey3 wget syslinux gptfdisk traceroute darkhttpd \
       dnsmasq dmidecode wireshark-cli nmap openvswitch openntpd \
       ntfs-3g ethtool intel-ucode vim-syntastic python-pip\
       python2-pip python-pylint python2-pylint flake8 python2-flake8 \
       vagrant qemu convmv clamav tk dosfstools hunspell-en iotop

printf "%s\n" "#### Install GUI mode packages ####"
# OPTIONAL: packages for graphical environment
pacman -S --noconfirm xfce4 xfce4-clipman-plugin lxdm chromium \
       firefox xarchiver meld gdmap deluge quodlibet gimp inkscape \
       simplescreenrecorder scrot motion xawtv gparted ibus \
       ibus-hangul ibus-qt pulseaudio pavucontrol gpa \
       virtualbox dkms virtualbox-guest-iso linux-headers \
       xf86-input-synaptics ttf-baekmuk wqy-bitmapfont wqy-zenhei \
       redshift zathura zathura-djvu jre8-openjdk virt-manager \
       calibre tigervnc xfce4-timer-plugin lxsession wireshark-qt \
       gpicview netdata lm-sensors nodejs fbterm evince xscreensaver \
       cups system-config-printer xsane terminator youtube-dl \
       smplayer audacity xfce4-notifyd tesseract tesseract-data-eng \
       tesseract-data-kor texlive-langcjk texlive-latexextra


printf "%s\n"   "###############################"
printf "%s\n"   "###    Set locale & time    ###"
printf "%s\n\n" "###############################"
printf "%s\n" "####  enable locales in /etc/locale.gen (en_US) ####"
sed -i "s:#en_US.UTF-8:en_US.UTF-8:g" /etc/locale.gen
printf "%s\n" "#### Generate locales ####"
locale-gen
printf "%s\n" "#### Set locale preferences in /etc/locale.conf ####"
echo "LANG=$LANG" > /etc/locale.conf

printf "%s\n" "#### Set time and date from PXE server ####"
date --set="$(ssh archjun@${pxeserver} date)"
hwclock -w


printf "%s\n"   "###############################"
printf "%s\n"   "###    Set Kernel Options   ###"
printf "%s\n\n" "###############################"


printf "%s\n" "#### Customize kernel modules in mkinitcpio.conf ####"
# Add hooks for 'systemd' and 'sd-lvm2' to /etc/mkinitcpio.conf
# to detect root part on LVM
sed -i 's:HOOKS="base udev autodetect modconf block filesystems keyboard fsck":HOOKS="base systemd sd-encrypt autodetect modconf block sd-lvm2 filesystems keyboard fsck":g' /etc/mkinitcpio.conf

printf "%s\n" "#### Regenerate initramfs image ####"
mkinitcpio -p linux


printf "%s\n"   "###############################"
printf "%s\n"   "###   Exit chroot, reboot   ###"
printf "%s\n\n" "###############################"
printf "%s\n" "#### Exit chroot ####"
printf "%s\n" "# exit"
printf "%s\n" "#### Steps to do after exiting chroot ####"
printf "%s\n" "# (1) umount -R /mnt"
printf "%s\n" "# (2) systemctl reboot"
