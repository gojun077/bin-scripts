#!/bin/bash
# chrootArchBIOS.sh
# Last Updated: 2017-01-10
# Jun Go gojun077@gmail.com

# This script should be run after 'installArchBIOS.sh'
# within the installation chroot. In the chroot,
# /mnt is mapped to /, so to scp this script to the
# chroot env, you must do the following:
#
# scp chrootArch.sh root@10.10.10.97:/mnt/
#
# Then the script should appear in the chroot '/'

pxeserver="10.10.10.97"

printf "%s\n" "#### Set hostname ####"
echo pcArch > /etc/hostname
printf "%s\n" "#### Set timezone ####"
ln -s /usr/share/zoneinfo/Asia/Seoul /etc/localtime


printf "%s\n"   "###############################"
printf "%s\n"   "###   Install add'l  pkgs   ###"
printf "%s\n\n" "###############################"
printf "%s\n" "#### Install text mode packages ####"
pacman -S --noconfirm screen mc vim cronie dialog wpa_supplicant \
       libnl git cmus mplayer fbida p7zip alsa-utils alsa-tools abs \
       emacs gdb shellcheck ipython2 ipython bitlbee ncdu \
       android-tools android-udev htop irssi dhclient openssh \
       firewalld nfs-utils vsftpd rsync docker intel-ucode auctex \
       archey3 wget syslinux gptfdisk traceroute darkhttpd \
       dnsmasq dmidecode wireshark-cli nmap openvswitch openntpd \
       ntfs-3g ethtool intel-ucode grub vim-syntastic python-pip\
       python2-pip python-pylint python2-pylint flake8 python2-flake8 \
       qemu vagrant convmv clamav tk dosfstools hunspell-en iotop

printf "%s\n" "#### Install GUI mode packages ####"
# OPTIONAL: packages for graphical environment
pacman -S --noconfirm xfce4 xfce4-clipman-plugin lxdm chromium \
       firefox xarchiver meld gdmap deluge quodlibet gimp inkscape \
       simplescreenrecorder scrot motion xawtv gparted ibus \
       ibus-hangul ibus-qt pulseaudio pavucontrol gpa \
       virtualbox dkms virtualbox-guest-iso linux-headers \
       xf86-input-synaptics ttf-baekmuk wqy-bitmapfont wqy-zenhei \
       redshift zathura zathura-djvu jre8-openjdk virt-manager \
       calibre tigervnc xfce4-timer-plugin lxsession wireshark-qt \
       gpicview netdata lm-sensors nodejs fbterm evince xscreensaver \
       cups system-config-printer xsane terminator youtube-dl \
       smplayer audacity xfce4-notifyd tesseract tesseract-data-eng \
       tesseract-data-kor texlive-langcjk texlive-latexextra


printf "%s\n"   "###############################"
printf "%s\n"   "###    Set locale & time    ###"
printf "%s\n\n" "###############################"
printf "%s\n" "####  enable locales in /etc/locale.gen (en_US) ####"
sed -i "s:#en_US.UTF-8:en_US.UTF-8:g" /etc/locale.gen
printf "%s\n" "#### Generate locales ####"
locale-gen
printf "%s\n" "#### Set locale preferences in /etc/locale.conf ####"
echo "LANG=$LANG" > /etc/locale.conf

printf "%s\n" "#### Set time and date from PXE server ####"
date --set="$(ssh archjun@${pxeserver} date)"
hwclock -w


printf "%s\n"   "###############################"
printf "%s\n"   "###    Set Kernel Options   ###"
printf "%s\n\n" "###############################"


printf "%s\n" "#### Customize kernel modules in mkinitcpio.conf ####"
# Add hooks for 'systemd' and 'sd-lvm2' to /etc/mkinitcpio.conf
# to detect root part on LVM
sed -i 's:HOOKS="base udev autodetect modconf block filesystems keyboard fsck":HOOKS="base systemd sd-encrypt autodetect modconf block sd-lvm2 filesystems keyboard fsck":g' /etc/mkinitcpio.conf

printf "%s\n" "#### Regenerate initramfs image ####"
mkinitcpio -p linux


printf "%s\n"   "###############################"
printf "%s\n"   "###    Set GRUB Options     ###"
printf "%s\n\n" "###############################"

sed -i 's:GRUB_CMDLINE_LINUX_DEFAULT="quiet":GRUB_CMDLINE_LINUX_DEFAULT="luks.uuid=72369889-3971-4039-ab17-f4db45aeeef2 root=UUID=f4dbfa30-35ba-4d5e-8d43-a7b29652f575 rw":g' /etc/default/grub

printf "%s\n" "### Install grub bootloader to /dev/sda ###"
grub-install --recheck /dev/sda
printf "%s\n" "### Generate grub.cfg ###"
grub-mkconfig -o /boot/grub/grub.cfg

printf "%s\n"   "###############################"
printf "%s\n"   "###   Exit chroot, reboot   ###"
printf "%s\n\n" "###############################"
printf "%s\n" "#### Exit chroot ####"
printf "%s\n" "# exit"
printf "%s\n" "#### Steps to do after exiting chroot ####"
printf "%s\n" "# (1) umount -R /mnt"
printf "%s\n" "# (2) systemctl reboot"
