#!/bin/bash
#ArchCleanup.sh
# Last Updated: 2016-10-04
# Jun Go gojun077@gmail.com

# This script removes all LV's, VG's, PV's as well as
# regular partitions and partition tables from a disk
# using GPT

FIRST_DISK=/dev/vda
MY_VG=ARCH

lvremove -f /dev/${MY_VG}/{homevol,rootvol,varvol,swapvol}
vgremove -f /dev/${MY_VG}
pvremove -f ${FIRST_DISK}
