#!/bin/bash
# post-installArchUser.sh
# Last Updated: 2016-10-05
# Jun Go gojun077@gmail.com

# Unlike post-installArch.sh, this script is intended to be run
# by the regular user. It should be executed after post-installArch.sh
# and will execute the following tasks:
# (1) Download AUR PKGBUILDs
# (2) Build PKGBUILDs with makepkg -s
# (3) Install the PKGBUILDs with pacman -U <pkg-name.xz>


user=archjun

###############################
###        VARIABLES        ###
###############################
TEMPF=$HOME/tempfile.txt
DROPBOX="dropbox.tar.gz"
DROPBOXCLI="dropbox-cli.tar.gz"
SPIDEROAK="spideroak-one.tar.gz"
SMTPCLI="smtp-cli.tar.gz"
XPRINTIDLE="xprintidle.tar.gz"
RESCUETIME="rescuetime.tar.gz"
TTFMONOFUR="ttf-monofur.tar.gz"
TTFNANUM="ttf-nanum.tar.gz"
TTFNANUMCODING="ttf-nanumgothic_coding.tar.gz"
MICROHEIKR="wqy-microhei-kr-patched.tar.gz"
TTFMSFONTS="ttf-ms-fonts.tar.gz"
MNEMOSYNE="mnemosyne.tar.gz"
IMGWRITER="imagewriter.tar.gz"
MYREPOS="myrepos.tar.gz"
MENULIBRE="menulibre.tar.gz"
PDFSHFLR="pdfshuffler-git.tar.gz"
PYPDF2="python-pypdf2.tar.gz"
PYKICK="python-pykickstart.tar.gz"
TTYCLK="tty-clock.tar.gz"

AURPKG=($DROPBOX
        $DROPBOXCLI
        $SPIDEROAK
        $SMTPCLI
        $XPRINTIDLE
        $RESCUETIME
        $TTFMONOFUR
        $TTFNANUM
        $TTFNANUMCODING
        $MICROHEIKR
        $TTFMSFONTS
        $MNEMOSYNE
        $IMGWRITER
        $MYREPOS
        $MENULIBRE
        $PYPDF2
        $PDFSHFLR
        $PYKICK
        $TTYCLK)


check_wget()
{
  FileName=$1
  URL=$2
  if ! [ -f "$HOME/Downloads/$FileName" ]; then
    wget "$URL"
  fi
}


printf "%s\n"   "###############################"
printf "%s\n"   "###     AUR PKGBUILD's      ###"
printf "%s\n\n" "###############################"
printf "%s\n" "#### Create Download Path ####"
mkdir -p "$HOME"/Downloads/AUR
DPATH="$HOME"/Downloads

# cd into $HOME/Downloads
cd "$DPATH" || exit 1

printf "%s\n" "#### wget Dropbox PKGBUILD ####"
check_wget $DROPBOX \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$DROPBOX

printf "%s\n" "#### wget Dropbox CLI PKGBUILD ####"
check_wget "$DROPBOXCLI" \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$DROPBOXCLI

printf "%s\n" "#### wget Spideroak One PKGBUILD ####"
check_wget "$SPIDEROAK" \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$SPIDEROAK

printf "%s\n" "#### wget smtp-cli PKGBUILD ####"
check_wget "$SMTPCLI" \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$SMTPCLI

printf "%s\n" "#### wget xprintidle PKGBUILD  (for Rescuetime ####"
check_wget "$XPRINTIDLE" \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$XPRINTIDLE

printf "%s\n" "#### wget Rescuetime PKGBUILD ####"
check_wget "$RESCUETIME" \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$RESCUETIME

printf "%s\n" "#### wget ttf-monofur PKGBUILD ####"
check_wget "$TTFMONOFUR" \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$TTFMONOFUR

printf "%s\n" "#### wget ttf-nanum (Naver) PKGBUILD ####"
check_wget "$TTFNANUM" \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$TTFNANUM

printf "%s\n" "#### wget ttf-nanumgothic_coding PKGBUILD ####"
check_wget "$TTFNANUMCODING" \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$TTFNANUMCODING

printf "%s\n" "#### wget wqy-microhei-kr-patched PKGBUILD ####"
check_wget $MICROHEIKR \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$MICROHEIKR

printf "%s\n" "#### wget ttf-ms-fonts PKGBUILD ####" # ttf-ms-fonts
check_wget $TTFMSFONTS \
           https://aur.archlinux.org/cgit/aur.git/snapshot/

printf "%s\n" "#### wget mnemosyne PKGBUILD ####"
check_wget $MNEMOSYNE \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$MNEMOSYNE

printf "%s\n" "#### wget SUSE imagewriter PKGBUILD ####"
check_wget $IMGWRITER \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$IMGWRITER

printf "%s\n" "#### wget myrepos PKGBUILD ####"
check_wget $MYREPOS \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$MYREPOS

printf "%s\n" "#### wget menulibre PKGBUILD ####"
check_wget $MENULIBRE \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$MENULIBRE

printf "%s\n" "#### wget menulibre PYPDF2 ####"
check_wget $PYPDF2 \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$PYPDF2

printf "%s\n" "#### wget pdfshuffler PKGBUILD  ####"
check_wget $PDFSHFLR \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$PDFSHFLR

printf "%s\n" "#### wget python-pykickstart PKGBUILD  ####"
check_wget $PYKICK \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$PYKICK

printf "%s\n" "#### wget tty-clock PKGBUILD  ####"
check_wget $TTYCLK \
           https://aur.archlinux.org/cgit/aur.git/snapshot/$TTYCLK

for i in ${AURPKG[*]}; do
  printf "%s\n" "Extract tarball $i to ~/Downloads/AUR/"
  cd "$DPATH" || exit 1
  tar -xvf "$i" -C "$HOME"/Downloads/AUR/
done

printf "%s\n" "### Write list of all dir's in ~/Downloads/AUR to file ###"
find ~/Downloads/AUR -maxdepth 1 -type d > "$TEMPF"
# NOTE: By default 'find' searches all dir's and sub-dir's recursively.
# the maxdepth option sets a limit to how deep 'find' will search, but
# even at maxdepth 0 or 1 it will always return the root path; i.e.
# the above cmd will always return '~/Downloads/AUR' first. I will
# therefore delete the first line from $TEMPF using 'sed'
sed -i '1d' "$TEMPF"

while read -r j; do
  DNAME=$(basename "$j")
  cd "$j" || exit 1
  printf "%s\n" "#### Build AUR PKG $DNAME  ####"
  makepkg -si --noconfirm
  #pacman -U --noconfirm "$DNAME"*.xz
done<"$TEMPF"


printf "%s\n"   "###############################"
printf "%s\n"   "###     SETUP SSH KEYS      ###"
printf "%s\n\n" "###############################"

printf "%s\n" "### Setup SpiderOak for retrieving SSH Keys ###"
SpiderOakONE --setup=-

if [ -d "$HOME/SpiderOak Hive/keys" ]; then
  printf "%s\n" "### Create symlinks from Spideroak to ~/.ssh ###"
  ln -s "$HOME/SpiderOak Hive/keys/ssh/archjun_rsa" "$HOME/.ssh/"
  ln -s "$HOME/SpiderOak Hive/keys/ssh/archjun_rsa.pub" "$HOME/.ssh/"
  printf "%s\n" "### Start ssh-agent ###"
  eval "$(ssh-agent)"
  printf "%s\n" "### Add private key archjun_rsa to ssh-agent ###"
  ssh-add "$HOME/.ssh/archjun_rsa"
  # The above private key is needed for cloning my private git repos
else
  printf "%s\n" "$HOME/SpiderOak Hive/keys does not exist"
  exit 1
fi


printf "%s\n"   "###############################"
printf "%s\n"   "###     CLONE GIT REPOS     ###"
printf "%s\n\n" "###############################"
printf "%s\n" "Create directories for cloned repos"
mkdir "$HOME"/Documents

printf "%s\n" "### Cloning 'dotfiles' repo ###"
git clone https://github.com/gojun077/jun-dotfiles.git \
    "$HOME"/dotfiles
printf "%s\n" "### Cloning 'bin-scripts' repo ###"
git clone https://gojun077@bitbucket.org/gojun077/bin-scripts.git \
    "$HOME"/bin

printf "%s\n" "### Set Dir perm's before setting up dotfiles ###"
sudo bash "$HOME/dotfiles/setACL_symlinks.sh" $user
printf "%s\n" "### Run create_symlinks.sh to setup dotfiles ###"
bash "$HOME/dotfiles/create_symlinks.sh"

### PRIVATE GIT REPOS
printf "%s\n" "### Cloning 'PXE' repo  ###"
git clone git@github.com:gojun077/pxe-config.git \
    /usr/local/tftpboot/pxelinux

printf "%s\n" "### Cloning 'growin_wo' repo  ###"
if [ -d "$HOME/Documents" ]; then
  mkdir "$HOME/Documents"
fi

git clone git@bitbucket.org:gojun077/growin_wo.git \
    "$HOME/Documents/growin_wo"

printf "%s\n" "### Cloning 'term_sessions' repo  ###"
git clone git@bitbucket.org:gojun077/term_sessions.git \
    "$HOME/Documents/term_sessions"

printf "%s\n" "### Cloning pomo2beeminder repo ###"
git clone git@github.com:gojun077/pomo2beeminder.git \
    "$HOME/Documents/"

printf "%s\n" "### Cloning 'junsTechNotes' repo  ###"
git clone git@gitlab.com:gojun077/junsTechNotes.git \
    "$HOME/Documents/"


printf "%s\n" "### Setup sshd.socket  ###"
sudo systemctl enable sshd.socket
sudo systemctl start sshd.socket

printf "%s\n" "### Copy syslinux files to pxelinux  ###"
cp /usr/lib/syslinux/bios/* /usr/local/tftpboot/pxelinux/
