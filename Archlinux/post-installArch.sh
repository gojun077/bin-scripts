#!/bin/bash
# post-installArch.sh
# Last Updated: 2016-12-20
# Jun Go gojun077@gmail.com

# This script should be run on a newly-installed
# Archlinux machine as root in the following order:
# (1) installArch.sh - pre-chroot setup
# (2) chrootArch.sh - chroot install tasks
# (3) post-installArch.sh - this script

# At first boot you can login as root w/o PW, but
# you should immediately set a root password and do the
# following:
# (a) Put up network iface and assign/get IP
# (b) systemctl start sshd.socket
# (c) create new non-root user and set password
#     - useradd -m foo
#     - passwd foo
# (d) from PXE server machine, SCP post-install script to new box


# Check that this script is being run as the root user
if [ "$USER" != "root" ]; then
  echo "This script must be executed as root (su -), not as sudo."
  exit 1
fi

MY_USER="archjun"
#DNS="168.126.63.1"
#PXE="10.10.10.97"

# printf "%s\n"   "###############################"
# printf "%s\n"   "###    SSH & Networking     ###"
# printf "%s\n\n" "###############################"
# printf "%s\n" "#### Enabling sshd.socket ####"
# systemctl enable sshd.socket
# systemctl start sshd.socket
printf "%s\n" "#### Enabling firewalld ####"
systemctl enable firewalld
systemctl start firewalld
# printf "%s\n" "Specify DNS in /etc/resolv.conf"
# echo "nameserver $DNS" >> /etc/resolv.conf
# printf "%s\n" "Make the PXE server the default gateway"
# ip r add default via $PXE


printf "%s\n"   "###############################"
printf "%s\n"   "###       User Setup        ###"
printf "%s\n\n" "###############################"
# printf "%s\n" "#### Create new user $MY_USER ####"
# useradd -m $MY_USER
# echo "$MY_USER:changeme" | chgpasswd

printf "%s\n" "#### Add $MY_USER to group 'wheel' ####"
usermod -a -G wheel ${MY_USER}
printf "%s\n" "#### Apply changes to group 'wheel' ####"
printf "%s\n" "#### Enable wheel group in '/etc/sudoers' ####"
sed -i 's:# %wheel ALL=(ALL) ALL:%wheel ALL=(ALL) ALL:g' /etc/sudoers
# Apply changes to wheel group in the current session
#newgrp wheel
if which virt-manager; then
  printf "%s\n" "#### Add $MY_USER to group 'libvirt' ####"
  usermod -a -G libvirt ${MY_USER}
else
  printf "%s\n" "## virt-manager not found, skipping libvirt group ##"
fi

if which wireshark; then
  printf "%s\n" "#### Add $MY_USER to group 'wireshark' ####"
  usermod -a -G libvirt ${MY_USER}
else
  printf "%s\n" "## wireshark not found; skipping wireshark group ##"
fi

if which virtualbox; then
  printf "%s\n" "#### Add $MY_USER to group 'vboxusers' ####"
  usermod -a -G vboxusers ${MY_USER}
else
  printf "%s\n" "## virtualbox not found; skipping vboxusers group ##"
fi

printf "%s\n" "#### Serial console access for regular user ####"
printf "%s\n" "# add group perms to access /dev/ttyX"
usermod -a -G tty ${MY_USER}
printf "%s\n" "# add group perms to access /dev/ttyUSBX"
usermod -a -G uucp ${MY_USER}
#printf "%s\n" "#### Apply changes to groups 'tty' and 'uucp' ####"
#newgrp tty
#newgrp uucp
if [ -d /MULTIMEDIA ]; then
  printf "%s\n" "#### Give $MY_USER write access on /MULTIMEDIA ####"
  chown -R $MY_USER.$MY_USER /MULTIMEDIA
else
  printf "%s\n" "/MULTIMEDIA mount point does not exist!"
fi


printf "%s\n"   "###############################"
printf "%s\n"   "###   Create Directories    ###"
printf "%s\n\n" "###############################"
printf "%s\n"   "#### Create sub-directories in /mnt ####"
mkdir /mnt/{distroIso,distroIso1,distroIso2,usb}
printf "%s\n"   "#### Create tftp directory in /usr/local ####"
mkdir -p /usr/local/tftpboot
printf "%s\n"   "#### Give $MY_USER ownership of /usr/local ####"
chown -R $MY_USER:$MY_USER /usr/local/
printf "%s\n"   "#### Create /root/tmp ####"
mkdir /root/tmp
