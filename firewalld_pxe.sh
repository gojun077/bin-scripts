#!/bin/bash
# firewalld_pxe.sh
# Created by Jun Go gojun077@gmail.com
# Last Updated 2016-12-28

# This script will open ports in firewalld needed for
# running a PXE server

# This script must be run as root

# This script has been tested on Archlinux and Fedora 24.

# USAGE
# sudo ./firewalld_pxe.sh <open/close>
# where <parameter> can be either 'open' or 'close'
# Parameter 'open' will cause the port or service to be
# added to the firewalld whitelist, while 'close' will
# cause them to be closed

if [ "$1" = "open" ]; then
  CMD=add;
elif [ "$1" = "close" ]; then
  CMD=remove
else
  echo -e "Parameter must be either 'open' or 'close'\n" 1>&2
  exit 1
fi

# DEFAULT FIREWALLD ZONE
#DZONE=$(firewall-cmd --get-default)
# Assume that the firewalld zone for PXE is zone 'internal'
# for wired interface
DZONE=internal
extzone=public

#################
#   NETWORK
#   IFACES
#################
# Check existence of network ifaces; if the iface exists but
# it is not included in the default firewalld zone, add it to
# the zone

IFACES=(br0
#        enp1s0
#        ens1
#        ens5
#        enp9s0
       )

for iface in ${IFACES[*]}; do
  if ip a | grep --silent "$iface"; then
    if ! firewall-cmd --list-all --zone $DZONE | grep --silent "$iface"; then
      firewall-cmd --add-interface "$iface" --zone $DZONE
    fi
  fi
done


#################
#   TCP PORTS
#################
# darkhttpd
DARK1=8080
DARK2=8000
# VNC Reverse Connection
VNCR=5500
LREPO=11112

TCPPORTS=($DARK1
          $DARK2
          $VNCR
          $LREPO
)


#################
#   UDP PORTS
#################
#DHCPSERV=68
#DHCPCLIENT=67
TFTP=69
BINL=4011

UDPPORTS=(
          $TFTP
          $BINL
)


#################
#   SERVICES
#################

SERVICES=(vnc-server
          http
          dhcp
          dns
)


#################
#   MAIN PROG
#################

# ENABLE SERVICES REQ'D FOR PXE
for i in ${SERVICES[*]}; do
  firewall-cmd --zone="$DZONE" --$CMD-service="$i"
done

# ENABLE TCP PORTS
for j in ${TCPPORTS[*]}; do
  firewall-cmd --zone="$DZONE" --$CMD-port="$j"/tcp
done

# ENABLE UDP PORTS
for k in ${UDPPORTS[*]}; do
  firewall-cmd --zone="$DZONE" --$CMD-port="$k"/udp
done

# ENABLE IP MASQUERADING
# This assumes that the PXE server gets internet access through
# a wireless interface while PXE services run on the wired
# iface
firewall-cmd --add-masquerade --zone $extzone

# List Firewall Zone Info (along with ports & svcs)
printf "%s\n" "### Listing firewalld rules for $DZONE ###"
firewall-cmd --list-all --zone $DZONE

printf "%s\n" "### Listing firewalld rules for $extzone ###"
firewall-cmd --list-all --zone $extzone
