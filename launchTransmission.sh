#!/bin/bash
# launchTransmission.sh
# Last updated 2016-12-24
# Jun Go gojun077@gmail.com

# Execute as 'root'
# - Open TCP 51413 in firewalld
# - Wait 5 sec.
# - while 'pidof transmission-gtk' returns '0'
#   noop
# - when 'pidof transmission-gtk' returns '1'
#   close TCP 51413 in firewalld

# Edit the default zone setting to match that
# of your machine
#DZONE=$(firewall-cmd --get-default)

zone="$1"

if [ $# -ne 1 ]; then
    printf "%s\n" "USAGE: sudo ./launchTransmission.sh [name of zone]"
    printf "%s\n" "ex: sudo ./launchTransmission.sh public"
    exit 1
fi

firewall-cmd --zone="$zone" --add-port 51413/tcp
sleep 5
while pidof transmission-gtk > /dev/null; do
  :
done

firewall-cmd --zone="$zone" --remove-port 51413/tcp
