#!/bin/bash
# r8822be_workaround.sh
#
# For realtek kernel driver issue in Ubuntu kernel 5.0.0-21,23
# Temporary workaround is to run this script as root


if ! [ "$USER" = "root" ]; then
  printf "%s\\n" "### This script must be executed as ROOT! ###"
  exit 1
fi

modprobe -r r8822be rtwpci
modprobe r8822be rtwpci
