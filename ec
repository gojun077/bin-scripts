#!/usr/local/bin/bash
# Start emacsclient and return immediately.
# from https://east.fm/posts/open-with-emacsclient/index.html
/Applications/Emacs.app/Contents/MacOS/bin/emacsclient -n -c "$@"

