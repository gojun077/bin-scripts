#!/usr/bin/env bash
# golang_install_linux.sh
#
# Created by: gojun077@gmail.com
# Created on: Dec 27 2021
# Last Updated: Jan 18 2024
#
# This script sets up a new Golang install from a tarball and sets up
# all required environment variables and paths. This script takes two
# arguments, 'username'.and 'url' for golang tarball download. This url
# is https://go.dev/dl/ as of Jul 2022

set -e
username="$1"
gourl="$2"


if [ $# -ne 2 ]; then
  printf "%s\\n" "### Please pass *username* as 1st arg! ###"
  printf "%s\\n" "Golang will be installed for that username only."
  printf "%s\\n" "### Please specify golang tarball DL url as 2nd arg! ###"
  exit 1
fi

# Note that go tarballs have the following naming convention:
# `go<version>.<os>-<arch>.tar.gz`
# version: x.yy.z, 'x' major ver, 'yy' minor ver, 'z' point ver
# os: one of 'darwin' (macOS), 'linux', 'windows', or 'freebsd'
# arch: one of 'amd64' (x86-64), 'arm64' (aarch64), '386' (x86), 'armv6l', etc
# example file: 'go1.21.6.darwin-amd64.tar.gz'

gopkg=$(basename "$gourl")
GOPATH="/home/$username/go"
GOROOT="/usr/local/go"
gopkg_os=""
gopkg_arch=""

if [[ "$gopkg" =~ "darwin" ]]; then
  gopkg_os=Darwin
fi
if [[ "$gopkg" =~ "linux" ]]; then
  gopkg_os=Linux
fi
if [[ "$gopkg" =~ "amd64" ]]; then
  gopkg_arch=x86_64
fi
if [[ "$gopkg" =~ "arm64" ]]; then
  gopkg_arch=aarch64
fi

checkos=$(python3 -c "import platform;mysys=platform.system();print(mysys)")
arch=$(python3 -c "import platform;myarch=platform.machine();print(myarch)")

if [[ "$checkos" == "$gopkg_os" && "$arch" == "$gopkg_arch" ]]; then
  :
else
  printf "%s\\n" \
         "## $gopkg incompatible with OS: $checkos Arch: $arch ! ##"
  exit 1
fi

if ! [ "$USER" = "root" ]; then
  printf "%s\\n" "### This script must be executed as ROOT! ###"
  exit 1
fi

if ! type wget &> /dev/null; then
  printf "%s\\n" "Please install wget before running this script"
  exit 1
fi

# Warn user and exit if $GOROOT already exists
if [ -d "$GOROOT" ]; then
  printf "%s\\n" \
         "$GOROOT already exists! Please run a golang upgrade instead."
  exit 1
fi

function download() {
  # refer to https://go.dev/dl/ for latest stable versions
  wget "$gourl" -P /tmp
}

function install() {
  printf "%s\\n" "### Extracting new Golang files to $GOROOT.. ###"
  tar -xvf /tmp/"$gopkg" -C /usr/local/
  mkdir -p "/home/$username/go"
  printf "%s\\n" "### Make sure to edit your PATH ###"
  printf "%s\\n" "Add $GOROOT/bin and $GOPATH/bin to your PATH"
  printf "%s\\n" "To make permanent, edit PATH in .bashrc or .bash_profile"
  printf "%s\\n" "### Change file permissions for $GOPATH ###"
  chown -R "$username":"$username" "$GOPATH"
}

# Only download tarball if it doesn't exist yet
if ! [ -f "/tmp/$gopkg" ]; then
  download
  install
else
  printf "%s\\n" "$gopkg already exists in /tmp!"
  read -r -p "Do you still want to upgrade? (y/n)" confirm
  if [[ $confirm == [yY] ]]; then
    install
  else
    printf "%s\\n" "Exiting..."
  fi
fi
