#!/bin/bash
# Script to setup wired interface on S310 work notebook
# Also adds a route to a server connected to the external
# network

# This script should be executed as root

# USAGE:
#sudo ./netsetup_work.sh <local_ip_w_subnet> <gateway_to_ext_network>

NIC=enp1s0

ip l set $NIC up
ip a flush dev $NIC
ip a add "$1" dev $NIC
# Add default route pointing to server with dual NIC's connected to
# both the internal and external networks. The server must have
# port forwarding and IP masquerading enabled for other
# machines on the internal network to get access to the
# external network via $2
ip r add default via "$2"
