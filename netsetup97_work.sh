#!/bin/bash
# netsetup97_work.sh

# Script to setup wired interface on S310 work notebook
# Calls netsetup_work.sh and provides local IP as
# well as IP of gateway server to bridge internal and
# external network

# This script should be executed as root
# and should be executed from ~/bin/

LOCAL=192.168.95.97/24
GW=192.168.95.145

./netsetup_work.sh $LOCAL $GW
