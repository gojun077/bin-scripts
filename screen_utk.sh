#!/bin/bash
# screen_utk.sh
#
# Created on: May 10 2021
# Created by: gojun077@gmail.com
# Last Updated: Aug 3 2023
#
# Set up a GNU Screen work environment for UTK

screen -AdmS utk           -t cmus      # tab 0
screen -S utk    -X screen -t sh0    # tab 1
screen -S utk    -X screen -t sh1    # tab 2
screen -S utk    -X screen -t sh2    # tab 3
screen -S utk    -X screen -t sh3    # tab 4
screen -S utk    -X screen -t rmt0      # tab 5
screen -S utk    -X screen -t rmt1      # tab 6
screen -S utk    -X screen -t argo      # tab 7
screen -S utk    -X screen -t musik     # tab 8

# tab 0
screen -S utk -p 0 -X stuff "cmus^M"
# tab 7
screen -S utk -p 7 -X stuff "ssh argonaut^M"
# tab 8
screen -S utk -p 8 -X stuff "musikcube^M"
