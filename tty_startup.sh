#!/bin/bash
# tty_startup.sh
# Last Updated: 2016-12-28
# Jun Go gojun077@gmail.com
#
# This script launches various daemons including `ssh-agent`
# if they are not already running
#

if pidof SpiderOakONE; then
  :
else
  SpiderOakONE --headless &
fi

if pidof dropbox; then
  :
else
  dropbox start &
fi

if pidof ssh-agent; then
  :
else
  eval "$(ssh-agent)"
  ssh-add ~/.ssh/archjun_rsa
fi
