#!/bin/bash
# ufw_pxe_cleanup.sh
# Jun Go gojun077@gmail.com
# Last Updated 2016-07-12

# This script will delete the ufw 'allow' rules
# created by ufw_pxe.sh and is called by
# pxe_cleanup.shj

# This script must be run as root

# This script has been tested on an Ubuntu 16.04
# server.

# USAGE
# sudo ./ufw_pxe_cleanup.sh
# No parameters

#################
#   TCP PORTS
#################
# darkhttpd
DARK1=8080
DARK2=8000
# ssh (this is enabled by default; no need to add)
#SSH=22

TCPPORTS=($DARK1
          $DARK2
         )


#################
#   UDP PORTS
#################
#DHCPSERV=68
#DHCPCLIENT=67
TFTP=69

UDPPORTS=($TFTP)


##################
#   BOTH TCP/UDP
##################
bootpc=68
bootps=67
http=80
BOTH=($bootpc
      $bootps
      $http
  )

#################
#   MAIN PROG
#################

# DELETE SERVICE RULES REQ'D FOR PXE
for i in ${UDPPORTS[*]}; do
  ufw delete allow "$i"/udp
done

# DELETE RULES FOR TCP
for j in ${TCPPORTS[*]}; do
  ufw delete allow "$j"/tcp
done

# DELETE RULES FOR BOTH TCP/UDP
for k in ${BOTH[*]}; do
  ufw delete allow "$k"
done

# List ports status
ufw status
