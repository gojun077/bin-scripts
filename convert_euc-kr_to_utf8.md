Linux Text Conversion Snippets
==============================

# Convert Text
==============
`iconv -c -f euc-kr -t utf8 file1.txt > file1-utf8.txt`
Where:
- `-c` silently discards unconvertable chars
- `-f` *from* encoding
- `-t` *to* encoding

# Convert Filename
==================
`env LANG=C 7za x -omyfolder archive.zip`
Where:
- `x` extract archive
- `-o` output path (no space after *-o*!)

convmv -f cp949 -t utf8 -r --notest /path/to/files
Where:
- `-f` from encoding
- `-t` to encoding
- `-r` convert recursively
- `--notest` actually rename files
