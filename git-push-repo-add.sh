#!/usr/bin/env bash
# git-push-repo-add.sh
#
# Created on: May 20 2024
# Created by: gopeterjun@naver.com
# Last Updated: May 20 2024
#
# This script takes a single argument of the form 'git@remotehost.com/org/repo-name'
# and stores this arg in var '$newrepo'. It then parses the output of 'git remote -v'
# and stores the existing (push) git repo in var '$oldrepo'. The script adds new
# push repo $newrepo and then re-adds the old push repo $oldrepo such that two push
# repos exist.


# Check if a command line argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <git@host/org/repo-name.git>"
    exit 1
fi

# Store the new remote repository URL
newrepo="$1"

# Check if there are multiple (push) remotes defined
num_push_remotes=$(git remote -v | grep -c '(push)')
if [ "$num_push_remotes" -gt 1 ]; then
    echo "You have multiple (push) remotes defined. This script only runs against git repos with 1 or less (push) remotes."
    exit 1
fi

# Parse the output of 'git remote -v' and store the (push) repo URL in $oldrepo
oldrepo=$(git remote -v | grep '(push)' | awk '{print $2}')

# Set the new remote repository URL as the (push) URL
git remote set-url origin --push --add "$newrepo"

# Add back the old (push) URL
git remote set-url origin --push --add "$oldrepo"

# Verify the remote URLs
echo "Remote URLs:"
git remote -v

