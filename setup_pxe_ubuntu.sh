#!/bin/bash
# setup_pxe_ubuntu.sh
# Jun Go gojun077@gmail.com
# Last Updated 2016-07-12

# This script installs pkgs, creates tftp root, copies syslinux
# boot images to paths under tftp root, copies netboot kernel
# and initrd images from Dropbox, clones repos, executes
# scripts, etc to create a usable PXE server environment on
# Ubuntu.
# NOTE: this script does not download Linux installation iso
# images; these must be prepared separately

# This script should be executed as the regular user, not root

TFTPBOOT="/usr/local/tftpboot/"
LUSER=junbuntu

# Create directories
if ! [ -d "$TFTPBOOT" ]; then
  sudo mkdir -p "$TFTPBOOT"
  sudo chown -R $LUSER:$LUSER $TFTPBOOT
fi

# Create path to store GNU Screen logs
if ! [ -d "$HOME"/Documents/term_sessions ]; then
  sudo mkdir -p "$HOME"/Documents/term_sessions
fi

# Create local tmp specified by vimrc
if ! [ -d "$HOME"/tmp ]; then
  sudo mkdir "$HOME"/tmp
fi


installpkg()
{
  # Check if 'nameOfPkg' is installed and install if not
  # USAGE: installpkg nameOfPkg
  if [ -z "$1" ]; then
    echo "You must enter the name of a package"
    exit 1
  else
    if ! dpkg -L "$1" 2&> /dev/null; then
      sudo apt-get install "$1"
    else
      echo "Package $1 is already installed"
    fi
  fi
}

PKGLIST="dnsmasq git pxelinux p7zip-full syslinux-common vim-runtime"

for i in $PKGLIST; do
  installpkg "$i"
done

### Clone repositories
# repos are public, so no password is needed
cd "$HOME" || exit 1
git clone 'https://gojun077@bitbucket.org/gojun077/bin-scripts.git' 'bin'
cd "$TFTPBOOT" || exit 1
git clone 'https://github.com/gojun077/pxe-config.git' 'pxelinux'

TFTPROOT="/usr/local/tftpboot/pxelinux"
### Copy pxe files to tftp-root dir
# netboot images from pkg 'pxelinux'
cp /usr/lib/PXELINUX/* $TFTPROOT/
# syslinux files from 'syslinux-common'
cp /usr/lib/syslinux/modules/bios/* $TFTPROOT/

### Copy kernel and initrd images from Dropbox shared folder
cd $TFTPROOT || exit 1
if ! [ -d $TFTPROOT/images ]; then
  wget -O dropbox.zip \
       https://www.dropbox.com/sh/b9dmtv1yxe46jt3/AACXVhzKv66IGM-gZ_atxABRa?dl=1
  7za x -oimages dropbox.zip
else
  echo "$TFTPROOT/images already exists"
fi

if [ -d images ]; then
  rm dropbox.zip
else
  echo "images subfolder was not created!"
fi

### Download config files PXE environment
cd "$HOME" || exit 1
# GNU Screen
wget -O .screenrc \
     https://raw.githubusercontent.com/gojun077/jun-dotfiles/96b44c0d349bc70aa0568078eb40d6e1a2cfe435/screenrc

# VIM
wget -O .vimrc \
     https://raw.githubusercontent.com/gojun077/jun-dotfiles/96b44c0d349bc70aa0568078eb40d6e1a2cfe435/vimrc
