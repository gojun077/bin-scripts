#!/bin/bash
# setup_vboxnet0_mac.sh
#
# Last Updated: 2017.11.13
# gojun077@gmail.com
#
# The default IP for the Virtualbox bridge iface on the host is
# 192.168.56.1 but sometimes VM guests using Vbox host networking
# want to use networks other than 192.168.56.0/24
# This script will set up vboxnet0 to use a different network
# address but assumes a 24-bit subnet.
# This script must be run as root.

netaddr="$1"

if [ $# -ne 1 ]; then
  printf "%s\n" "### Must enter IP address for vboxnet0 ###"
  exit 1
fi

ipconfig set vboxnet0 INFORM "$netaddr" 255.255.255.0
route get "$netaddr"/24

