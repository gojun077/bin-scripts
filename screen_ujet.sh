#!/bin/bash
# screen_ujet.sh
#
# Created on: 30 Apr 2024
# Created by: gopeterjun@naver.com
# Last Updated: 22 Oct 2024
#
# Set up a GNU Screen work environment for UJET

screen -AdmS ujet          -t cmus   # tab 0
screen -S ujet    -X screen -t sh0    # tab 1
screen -S ujet    -X screen -t sh1    # tab 2
screen -S ujet    -X screen -t sh2    # tab 3
screen -S ujet    -X screen -t jira   # tab 4
screen -S ujet    -X screen -t rmt0   # tab 5
screen -S ujet    -X screen -t rmt1   # tab 6
screen -S ujet    -X screen -t util   # tab 7
screen -S ujet    -X screen -t ovpn   # tab 8

# tab 0
screen -S ujet -p 0 -X stuff "cmus^M"
# tab 1 - add all ssh keys in ~/.ssh to ssh-agent
screen -S ujet -p 1 -X stuff 'grep -slR "PRIVATE" ~/.ssh/ | xargs ssh-add^M'
# tab 7: utils
screen -S ujet -p 7 -X stuff 'eval "$(op signin)"'
screen -S ujet -p 8 -X stuff "# execute openvpn3 script^M"
