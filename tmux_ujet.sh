#! /usr/bin/env bash
# tmux_termux.sh
#
# Created by: gojun077@gmail.com
# Created on: 04 Jul 2024
# Last Updated: 13 Dec 2024
#
# This is a startup script that sets up a tmux multiplexer envo
# for UJET on work laptop. This multiplexer runs alongside a
# GNU Screen session and is used for running ncurses apps like
# 'k9s', 'mc', etc.


session="ujet"

# check for other tmux sessions with the same name
if pgrep -x "tmux" >/dev/null; then
    if (tmux list-sessions | grep -q "$session"); then
        printf "%s\\n" "tmux session $session already exists!"
        exit 1
    fi
fi

printf "%s\\n" "launching tmux session $session in detached mode..."
tmux new -s "$session" -d

printf "%s\\n" "setting up window 0..."
tmux rename-window -t 0 'loc0'
# Setup main window 'loc0'
tmux send-keys -t 'loc0' 'printf "%s\\n" "This is pane 0"' C-m

printf "%s\\n" "setting up window 1..."
# Create and setup pane for local terminal
tmux new-window -t $session:1 -n 'loc1'
tmux send-keys -t 'loc1' 'printf "%s\\n" "This is pane 1"' C-m

printf "%s\\n" "setting up window 2..."
# Create and setup pane for k9s
tmux new-window -t $session:2 -n 'k9s'
tmux send-keys -t 'k9s' 'k9s' # don't send <ENTER> with `C-m`

printf "%s\\n" "setting up window 3..."
# Create and setup pane for musikcube
tmux new-window -t $session:3 -n 'musikc'
tmux send-keys -t 'musikc' 'musikcube' C-m

