#!/bin/bash
#===========================================
# meditationBeeminder.sh created 2016.01.04
# https://bitbucket.org/gojun077/bin-scripts
# Jun Go gojun077@gmail.com
#===========================================
# This script is designed to be launched by xfce4-timer-plugin
# or pystopwatch after a meditation session is completed
# The user will be asked whether or not the meditation was completed
# successfully (without distractions). If the answer is 'y', the
# script will launch 'smtp-cli' which will send an email to the
# Beeminder bot to increment a Beeminder graph for a 'do more'
# goal named 'meditation'. This script is almost identical to
# pomo2beeminder.sh except the email SUBJ is different to account
# for the different goal name ("meditation" instead of "pomodoro")
#
# This script has been tested with smtp-cli 3.6 and 3.7
# https://github.com/mludvig/smtp-cli (3.7)
# http://www.logix.cz/michal/devel/smtp-cli/ (3.6)

MBPATH="$HOME"/bin
HOST=smtp.gmail.com
USER=gojun077@gmail.com
PW=$(<"$MBPATH"/gmail_pass.txt)
FROM=gojun077@gmail.com
TO=bot@beeminder.com
SUBJ="gojun077/meditation"

ans=z  #initialize variable 'ans'

while [[ "$ans" != "y" || "$ans" != "n" ]]; do
  echo "Did you successfully complete your meditation?(y/n)"
  read -r ans

  case $ans in
    "y")
         if [ -f "/usr/bin/smtp-cli" ]; then
           smtp-cli --verbose --host=$HOST --enable-auth --user=$USER \
                    --pass="$PW" --from=$FROM --to=$TO --subject=$SUBJ \
                    --body-plain='^ 1 "sent by meditationBeeminder.sh"' \
                    --charset=UTF-8
           break
         else
           echo -e "Please install 'smtp-cli' package! Aborting.\n" 1>&2
           exit 1
         fi
         ;;
    "n")
         echo "Concentrate harder next time!"
         break
         ;;
    *)
         echo "Please answer y or n"
         ;;
  esac
done
