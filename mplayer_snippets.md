Jun's mplayer snippets
======================

- Shuffle play all audio files in a directory, looping forever
  + `mplayer -shuffle -loop 0 *`
