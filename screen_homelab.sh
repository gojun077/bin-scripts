#!/bin/bash
# screen_homelab.sh
#
# Created on: 01 Aug 2024
# Created by: gopeterjun@naver.com
# Last Updated: 01 Aug 2024
#
# Set up a GNU Screen envo for connecting to my homelab instances

screen -AdmS homelab        -t cmus   # tab 0
screen -S homelab -X screen -t sh0    # tab 1
screen -S homelab -X screen -t sh1    # tab 2
screen -S homelab -X screen -t sh2    # tab 3
screen -S homelab -X screen -t sh3    # tab 4
screen -S homelab -X screen -t rmt0   # tab 5
screen -S homelab -X screen -t rmt1   # tab 6
screen -S homelab -X screen -t rmt2   # tab 7
screen -S homelab -X screen -t rmt3   # tab 8

# tab 0
screen -S homelab -p 0 -X stuff "cmus^M"
# tab 1
screen -S homelab -p 1 -X stuff 'grep -slR "PRIVATE" ~/.ssh/ | xargs ssh-add^M'
# tab 5 
screen -S homelab -p 5 -X stuff "# remote 0^M"
# tab 6 
screen -S homelab -p 6 -X stuff "# remote 1^M"
# tab 7
screen -S homelab -p 7 -X stuff "# remote 2^M"
# tab 8: do not send newline; 2FA login requires browser auth by human
screen -S homelab -p 8 -X stuff "# remote 3^M"
