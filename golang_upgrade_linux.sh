#!/usr/bin/env bash
# golang_upgrade_linux.sh
#
# Created by: gojun077@gmail.com
# Created on: Dec 30 2021
# Last Updated: Jan 18 2024
#
# This script updates an existing Golang install that was setup with
# 'golang_install*.sh'. This script takes a single argument,
# <gourl>, the url to the golang tarball for Linux.


set -e
gourl="$1"


if [ $# -ne 1 ]; then
  printf "%s\\n" "### Please pass 1 argument to this script! ###"
  printf "%s\\n" "1: url for downloading golang tarball for Linux"
  exit 1
fi

# Note that go tarballs have the following naming convention:
# `go<version>.<os>-<arch>.tar.gz`
# version: x.yy.z, 'x' major ver, 'yy' minor ver, 'z' point ver
# os: one of 'darwin' (macOS), 'linux', 'windows', or 'freebsd'
# arch: one of 'amd64' (x86-64), 'arm64' (aarch64), '386' (x86), 'armv6l', etc
# example file: 'go1.21.6.darwin-amd64.tar.gz'

gopkg=$(basename "$gourl")
gopkg_os=""
gopkg_arch=""

if [[ "$gopkg" =~ "darwin" ]]; then
  gopkg_os=Darwin
fi
if [[ "$gopkg" =~ "linux" ]]; then
  gopkg_os=Linux
fi
if [[ "$gopkg" =~ "amd64" ]]; then
  gopkg_arch=x86_64
fi
if [[ "$gopkg" =~ "arm64" ]]; then
  gopkg_arch=aarch64
fi

gopkg=$(basename "$gourl")
checkos=$(python3 -c "import platform;mysys=platform.system();print(mysys)")
arch=$(python3 -c "import platform;myarch=platform.machine();print(myarch)")

if [[ "$checkos" == "$gopkg_os" && "$arch" == "$gopkg_arch" ]]; then
  :
else
  printf "%s\\n" \
         "## $gopkg incompatible with OS: $checkos Arch: $arch ! ##"
  exit 1
fi

if ! [ "$USER" = "root" ]; then
  printf "%s\\n" "### This script must be executed as ROOT! ###"
  exit 1
fi

if ! type wget &> /dev/null; then
  printf "%s\\n" "Please install wget before running this script"
  exit 1
fi

function download() {
  wget "$gourl" -P /tmp
}

function upgrade() {
  printf "%s\\n" "### Deleting Old Golang pkgs from '/usr/local/go' ###"
  rm -rf /usr/local/go

  printf "%s\\n" "### Extracting New Golang pkgs to '/usr/local/go' ###"
  tar -xvf "/tmp/$gopkg" -C /usr/local/
}

# Only download tarball if it doesn't exist yet
if ! [ -f "/tmp/$gopkg" ]; then
  download
  upgrade
else
  printf "%s\\n" "$gopkg already exists in /tmp!"
  read -r -p "Do you still want to upgrade? (y/n)" confirm
  if [[ $confirm == [yY] ]]; then
    upgrade
  else
    printf "%s\\n" "Exiting..."
  fi
fi
