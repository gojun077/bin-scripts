#!/bin/bash
#
# screen_mac.sh
#
# Last Updated: 2020.05.11
# Updated by: gojun077@gmail.com
#
# Sets up a a GNU Screen work environment for MacOS complete with
# named tabs

screen -AdmS work           -t home     # tab 0 'home'
screen -S work    -X screen -t fifa     # tab 1
screen -S work    -X screen -t pict     # tab 2
screen -S work    -X screen -t loc0     # tab 3 local0
screen -S work    -X screen -t loc1     # tab 4 local1
screen -S work    -X screen -t rmt0     # tab 5 remote0
screen -S work    -X screen -t rmt1     # tab 6 remote1
screen -S work    -X screen -t rmt2     # tab 7 remote2
screen -S work    -X screen -t bitlbee  # tab 8 IRC bridge
screen -S work    -X screen -t irssi    # tab 9 irssi IRC client
screen -S work    -X screen -t cmus     # tab 10 CLI music player

# tab 1
screen -S work -p 1 -X stuff "cd ~/Documents/eak-fifa-devops^M"
# tab 2
screen -S work -p 2 -X stuff "cd ~/Desktop^M"
# tab 3; 10.96.0.0/13 covers 10.96.0.0 to 10.103.255.255
screen -S work -p 3 -X stuff "sshuttle -r toolz-cmd -v 10.96.0.0/13"
# tab 4
screen -S work -p 4 -X stuff "echo 'hello world'^M"
# tab 8 bitlbee
screen -S work -p 8 -X stuff "bitlbee^M"
# tab 9 launch irssi
screen -S work -p 9 -X stuff "irssi^M"
# tab 10 launch cmus
screen -S work -p 10 -X stuff "cmus^M"

