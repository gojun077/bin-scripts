 Launch Examples for Screen PXE Server Script
=============================================+

# Open ports in firewalld, use ks, server iso, use vnc reverse connect

> Example for Joshua Huh at Whalex/GDAC. The iso is CentOS 7.4;
> Use a kickstart that is CentOS 7.4-related

```sh
./screen_pxe.sh -e br0@10.10.10.97/24 -f firewalld \
-k /usr/local/tftpboot/pxelinux/kickstart-pxe/ \
-i /MULTIMEDIA/iso/CentOS-7-x86_64-DVD-1708.iso -v
```

## Notes
  - for multiple VM's, remove '-v' option for vnc reverse-connect
  - recent versions of tigervnc only support a single reverse connect
  - for multiple vncserver connections connect manually
    + vncclient IP:1
  - to get IP, observe dhcp handshake in syslog

# For server room ubuntu 16.04 pxe server:

```
./screen_pxe.sh -e enp4s0@192.168.95.148/24 -f ufw \
-k /usr/local/tftpboot/pxelinux/kickstart-pxe/ \
-i $HOME/Downloads/ubuntu-16.04-server-amd64.iso
```

# For testing PXE with single KVM guest:
./screen_pxe.sh -e br0@192.168.95.97/24 -f firewalld \
-k /usr/local/tftpboot/pxelinux/kickstart-pxe/ \
-i /MULTIMEDIA/iso/rhel-server-7.2-x86_64-dvd.iso -v

## Notes
  - Don't forget to add iface `br0` to the default firewalld zone

# For testing UEFI PXE on ASUS U36JC

```
./screen_pxe.sh -e br0@192.168.95.97/24 -f firewalld \
-k /usr/local/tftpboot/pxelinux/kickstart-pxe/ \
-i /MULTIMEDIA/iso/Fedora-Server-dvd-x86_64-24-1.2.iso -v
```

# Miscellaneous
  - If you wish to manually mount the install iso it must be mounted on:
    + `/mnt/distroIso`
