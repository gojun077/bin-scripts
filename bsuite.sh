#!/bin/bash
# bsuite.sh
#
# Last Updated: 2019-08-31
# Jun Go gojun077@gmail.com
#
# Launch Script for Burp Suite .jar file


BSPATH=$HOME/burpsuite
XMODIFIERS=@im=ibus java -jar -Xmx4096M \
          -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel \
          "$BSPATH"/burpsuite_community_v2.1.02.jar
