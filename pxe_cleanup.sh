#!/bin/bash
# This cleanup script should be run as root. It will clean up
# after you are finished running a PXE server launched by
# screen_pxe.sh

if grep "/mnt/distroIso" /etc/mtab > /dev/null; then
    umount /mnt/distroIso
else
  echo "no iso is mounted on /mnt/distroIso"
fi

if pidof dnsmasq > /dev/null; then
    systemctl stop dnsmasq
else
    echo "dnsmasq is not running"
fi

if pidof vncviewer > /dev/null; then
    kill -15 "$(pidof vncviewer)"
else
    echo "vncviewer is not running"
fi

if pidof darkhttpd > /dev/null; then
    kill -15 "$(pidof darkhttpd)"
else
    echo "darkhttpd is not running"
fi

### Close ports opened in the firewall for pxe
# UFW
if [ -f /usr/sbin/ufw ]; then
  ./ufw_pxe_cleanup.sh
# FIREWALLD
else
  ./firewalld_pxe.sh close
fi

